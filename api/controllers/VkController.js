var Promise = require("bluebird");
var fs = require('fs');
var Download = require('download');
var archiver = require('archiver');
var archive = archiver.create('zip', {});

var writeFile = Promise.promisify(fs.writeFile);

var photoSizes = ['src_xxxbig', 'src_xxbig', 'src_xbig', 'src_big', 'src', 'src_small'];

module.exports = {
  getPhotos: function(req, res) {
    var time = Date.now();
    fs.mkdirSync(sails.config.appPath + '/assets/export/' + req.body[0].owner_id + '-' + time);
    var path = sails.config.appPath + '/assets/export/' + req.body[0].owner_id + '-' + time + '/';
    var dataFile = path + 'data.txt';
    writeFile(dataFile, JSON.stringify(req.body)).then(function() {
//      res.json({
//        url: dataFile.replace(sails.config.appPath + '/assets', '')
//      });
      req.body.forEach(function(album) { 
        fs.mkdirSync(path + album.title);
      });
      var download = new Download();
      req.body.forEach(function(album) {
        album.photos.forEach(function(photo) {
          var biggestPhotoUrl;
          photoSizes.forEach(function(size) {
            if (photo[size] && !biggestPhotoUrl) {
              biggestPhotoUrl = photo[size];
            }
          });
          download.get(biggestPhotoUrl, path + album.title);
        });
      });
      download.run(function(err, files) {
        if (err) { console.log(err); }
        console.log('DOWNLOAD DONE!');
        var zipStream = fs.createWriteStream(sails.config.appPath + '/assets/export/' + 'export-' + req.body[0].owner_id + '-' + time + '.zip');
        zipStream.on('close', function() {
          console.log('ARCHIVE DONE!');
          res.json({
            dataFile: dataFile.replace(sails.config.appPath + '/assets', ''),
            export: '/export/export-' + req.body[0].owner_id + '-' + time + '.zip'
          });
        });
        archive.on('error', function(err) {
          console.log(err);
        });
        archive.pipe(zipStream);
        archive.directory(sails.config.appPath + '/assets/export/' + req.body[0].owner_id + '-' + time, req.body[0].owner_id + '-' + time);
        archive.finalize();
      });
    });
  }
};


